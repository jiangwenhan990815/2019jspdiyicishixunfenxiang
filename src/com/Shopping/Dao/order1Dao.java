package com.Shopping.Dao;

import java.util.ArrayList;

public interface order1Dao {
	//查询全部信息
		public ArrayList getList();
		
		//增
		public void add(int orderid,String buyer,String time);
		//删
		public void delete(String user);
		//改
		public void update(String pwd,String user) ;
		
		public int length();//获取数据库的长度
}