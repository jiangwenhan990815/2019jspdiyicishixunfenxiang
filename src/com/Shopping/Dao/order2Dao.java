package com.Shopping.Dao;

import java.util.ArrayList;

import com.Shopping.Server.shoporder2;

public interface order2Dao {
	public ArrayList<shoporder2> getList();
	public void add(int orderid,int id,String name,String img,double price,int num,String seller);
	public void delete(String user);
	public void update(String pwd,String user);
	public int length();
	
}
