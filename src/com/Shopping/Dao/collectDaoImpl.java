package com.Shopping.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Shopping.Server.collect;


public class collectDaoImpl extends BaseDao implements collectDao{
	ResultSet rs=null;
	Connection cnt=null;
	PreparedStatement ps=null;
	
	//查询全部信息
	@Override
	public ArrayList<collect> getList() {
		ArrayList<collect> array = new ArrayList<collect>();
		String sql="select * from collect";//查询表全部信息
		//初始化 数组 ，避免空指针
		Object[] params={};
		rs=this.ExecuteQuery(sql, params);
		
		//在控制台输出 rs的结果集
		try {
			while(rs.next()){
				collect us = new collect();
				us.setName1(rs.getString(1));
				us.setName2(rs.getString(2));
				us.setRelation(rs.getString(3));
				array.add(us);
			}
			return array;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			this.closeResource();
		}
		return array;
	}
 
	//增
	@Override
	public void add(String name1,String name2,String relation) {
		String sql="insert into collect(name1,name2,relation) values(?,?,?)";
		Object [] params={name1,name2,relation};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("添加成功");
		}
	}
 
	//删除
	@Override
	public void delete(String name1,String name2) {
		String sql="delete from collect where name1=? and name2=?";
		Object [] params={name1,name2};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("删除成功");
		}
	}
	
//修改
	@Override
	public void update() {
		String sql="update collect set pass=? where email=?";
		Object [] params={};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("修改成功");
			
		}
	}
	public int length() {
		String sql = "SELECT * FROM collect;";
		int i = this.lengthrow(sql);
		System.out.println(i);
		return i;
		
	}
 
}
