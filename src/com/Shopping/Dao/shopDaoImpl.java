package com.Shopping.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Shopping.Server.shopInf;


public class shopDaoImpl extends BaseDao implements shopDao{
	ResultSet rs=null;
	Connection cnt=null;
	PreparedStatement ps=null;
	
	//查询全部信息
	@Override
	public ArrayList<shopInf> getList() {
		ArrayList<shopInf> array = new ArrayList<shopInf>();
		String sql="select * from shopping";//查询表全部信息
		//初始化 数组 ，避免空指针
		Object[] params={};
		rs=this.ExecuteQuery(sql, params);
		
		//在控制台输出 rs的结果集
		try {
			while(rs.next()){
				shopInf si = new shopInf();
				si.setId(rs.getInt(1));
				si.setName(rs.getString(2));
				si.setSeller(rs.getString(3));
				si.setPrice(rs.getDouble(4));
				si.setNum(rs.getInt(5));
				si.setType(rs.getString(6));
				si.setLocal(rs.getString(7));
				array.add(si);
			}
			return array;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			this.closeResource();
		}
		return array;
	}
 
	//增
	@Override
	public void add(int id,String name,String seller,double price,int num,String type,String local) {
		String sql="insert into shopping(id,name,seller,price,num,type,local) values(?,?,?,?,?,?,?)";
		Object [] params={id,name,seller,price,num,type,local};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("添加成功");
		}
	}
 
	//删除
	@Override
	public void delete(String user) {
		String sql="delete from userInf where user=?";
		Object [] params={user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("删除成功");
		}
	}
	
//修改
	@Override
	public void update(String pwd,String user) {
		String sql="update userInf set pwd=? where user=?";
		Object [] params={pwd,user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("修改成功");
			
		}
	}
	public int length() {
		String sql = "SELECT * FROM shopping.shopping;";
		int i = this.lengthrow(sql);
		System.out.println(i);
		return i;
		
	}
 
}



