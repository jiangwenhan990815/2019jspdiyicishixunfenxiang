package com.Shopping.Dao;


import java.util.ArrayList;
import java.util.Date;
   //接口类
public interface shopDao {
	//查询全部信息
	public ArrayList getList();
	
	//增
	public void add(int id,String name,String seller,double price,int num,String type,String local);
	//删
	public void delete(String user);
	//改
	public void update(String pwd,String user);
	
	public int length();//获取数据库的长度
}