package com.Shopping.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Shopping.Server.shoporder1;


public class order1DaoImpl extends BaseDao implements order1Dao{
	ResultSet rs=null;
	Connection cnt=null;
	PreparedStatement ps=null;
	
	//查询全部信息
	@Override
	public ArrayList<shoporder1> getList() {
		ArrayList<shoporder1> array = new ArrayList<shoporder1>();
		String sql="select * from shoporde";//查询表全部信息
		//初始化 数组 ，避免空指针
		Object[] params={};
		rs=this.ExecuteQuery(sql, params);
		
		//在控制台输出 rs的结果集
		try {
			while(rs.next()){
				shoporder1 so = new shoporder1();
				so.setOrderid(rs.getInt(1));
				so.setBuyer(rs.getString(2));
				so.setTime(rs.getString(3));
				array.add(so);
			}
			return array;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			this.closeResource();
		}
		return array;
	}
 
	//增
	@Override
	public void add(int orderid,String buyer,String time) {
		String sql="insert into shoporde(orderid,buyer,time) values(?,?,?)";
		Object [] params={orderid,buyer,time};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("添加成功");
		}
	}
 
	//删除
	@Override
	public void delete(String user) {
		String sql="delete from shoporde where user=?";
		Object [] params={user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("删除成功");
		}
	}
	
//修改
	@Override
	public void update(String pwd,String user) {
		String sql="update shoporde set pwd=? where user=?";
		Object [] params={pwd,user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("修改成功");
			
		}
	}
	public int length() {
		String sql = "SELECT * FROM shopping.shoporde;";
		int i = this.lengthrow(sql);
		System.out.println(i);
		return i;
		
	}
 
}



