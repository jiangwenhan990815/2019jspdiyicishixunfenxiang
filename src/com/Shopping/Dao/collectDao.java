package com.Shopping.Dao;

import java.util.ArrayList;

import com.Shopping.Server.collect;

public interface collectDao {

	public int length() ;
	public void update();
	public void delete(String name1,String name2) ;
	public void add(String name1,String name2,String relation);
	public ArrayList<collect> getList() ;
}
