package com.Shopping.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Shopping.Server.imgInf;


public class imgDaoImpl extends BaseDao implements imgDao{
	ResultSet rs=null;
	Connection cnt=null;
	PreparedStatement ps=null;
	
	//查询全部信息
	@Override
	public ArrayList<imgInf> getList() {
		ArrayList<imgInf> array = new ArrayList<imgInf>();
		String sql="select * from shopImg";//查询表全部信息
		//初始化 数组 ，避免空指针
		Object[] params={};
		rs=this.ExecuteQuery(sql, params);
		
		//在控制台输出 rs的结果集
		try {
			while(rs.next()){
				imgInf ii = new imgInf();
				ii.setId(rs.getInt(1));
				ii.setImg(rs.getString(2));
				ii.setIfShow(rs.getInt(3));
				array.add(ii);
			}
			return array;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			this.closeResource();
		}
		return array;
	}
 
	//增
	@Override
	public void add(int id,String img,int ifShow) {
		String sql="insert into shopImg(id,img,ifShow) values(?,?,?)";
		Object [] params={id,img,ifShow};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("添加成功");
		}
	}
 
	//删除
	@Override
	public void delete(String user) {
		String sql="delete from userInf where user=?";
		Object [] params={user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("删除成功");
		}
	}
	
//修改
	@Override
	public void update(String pwd,String user) {
		String sql="update userInf set pwd=? where user=?";
		Object [] params={pwd,user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("修改成功");
			
		}
	}
	public int length() {
		String sql = "SELECT * FROM shopping.shopImg;";
		int i = this.lengthrow(sql);
		System.out.println(i);
		return i;
		
	}
 
}




