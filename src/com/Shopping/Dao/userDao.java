package com.Shopping.Dao;



import java.util.ArrayList;
import java.util.Date;
   //接口类
public interface userDao {
	//查询全部信息
	public ArrayList getList();
	
	//增
	public void add(String user,String pass,String way,String email,String img);
	//删
	public void delete(String user);
	//改
	public void update(String pwd,String user);//修改密码
	
	public void updateimg(String user,String img)	; //修改头像
	
	public int length();//获取数据库的长度
}