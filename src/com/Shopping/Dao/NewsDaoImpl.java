package com.Shopping.Dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
 
import com.Shopping.Dao.BaseDao;
import com.Shopping.Dao.NewsDao;
import com.Shopping.Server.userInf;
 
public class NewsDaoImpl extends BaseDao implements NewsDao{
	ResultSet rs=null;
	Connection cnt=null;
	PreparedStatement ps=null;
	
	//查询全部信息
	@Override
	public ArrayList<userInf> getList() {
		ArrayList<userInf> array = new ArrayList<userInf>();
		String sql="select * from userInf";//查询表全部信息
		//初始化 数组 ，避免空指针
		Object[] params={};
		System.out.println("dada");
		rs=this.ExecuteQuery(sql, params);
		
		//在控制台输出 rs的结果集
		try {
			while(rs.next()){
				userInf ui = new userInf();
				ui.setUser(rs.getString(1));
				ui.setPass(rs.getString(2));
				ui.setWay(rs.getString(3));
				ui.setEmail(rs.getString(4));
				ui.setPhone(rs.getString(5));
				ui.setImg(rs.getString(6));
				array.add(ui);
			}
			return array;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			this.closeResource();
		}
		return array;
	}
 
	//增
	@Override
	public void add(String user,String pass,String way,String email,String phone,String img) {
		String sql="insert into userInf(user,pwd,way,email,phone,img) values(?,?,?,?,?,?)";
		Object [] params={user,pass,way,email,phone,img};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("添加成功");
		}
	}
 
	//删除
	@Override
	public void delete(String user) {
		String sql="delete from userInf where user=?";
		Object [] params={user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("删除成功");
		}
	}
	
//修改
	@Override
	public void update(String pwd,String user) {
		String sql="update userInf set pwd=? where user=?";
		Object [] params={pwd,user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("修改成功");
			
		}
	}
	
	public int length() {
		String sql = "SELECT * FROM shopping.userInf;";
		int i = this.lengthrow(sql);
		System.out.println(i);
		return i;
		
	}
 
}


