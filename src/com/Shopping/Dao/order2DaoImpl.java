package com.Shopping.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Shopping.Server.shoporder2;

public class order2DaoImpl extends BaseDao implements order2Dao{
	ResultSet rs=null;
	Connection cnt=null;
	PreparedStatement ps=null;
	
	//查询全部信息
	@Override
	public ArrayList<shoporder2> getList() {
		ArrayList<shoporder2> array = new ArrayList<shoporder2>();
		String sql="select * from shoporder";//查询表全部信息
		//初始化 数组 ，避免空指针
		Object[] params={};
		rs=this.ExecuteQuery(sql, params);
		
		//在控制台输出 rs的结果集
		try {
			while(rs.next()){
				shoporder2 so = new shoporder2();
				so.setOrderid(rs.getInt(1));
				so.setId(rs.getInt(2));
				so.setName(rs.getString(3));
				so.setImg(rs.getString(4));
				so.setPrice(rs.getDouble(5));
				so.setNum(rs.getInt(6));
				so.setSeller(rs.getString(7));
				array.add(so);
			}
			return array;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			this.closeResource();
		}
		return array;
	}
 
	//增
	@Override
	public void add(int orderid,int id,String name,String img,double price,int num,String seller) {
		String sql="insert into shoporder(orderid,id,name,img,price,num,seller) values(?,?,?,?,?,?,?)";
		Object [] params={orderid,id,name,img,price,num,seller};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("添加成功");
		}
	}
 
	//删除
	@Override
	public void delete(String user) {
		String sql="delete from shoporder where user=?";
		Object [] params={user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("删除成功");
		}
	}
	
//修改
	@Override
	public void update(String pwd,String user) {
		String sql="update shoporder set pwd=? where user=?";
		Object [] params={pwd,user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("修改成功");
			
		}
	}
	public int length() {
		String sql = "SELECT * FROM shopping.shoporder;";
		int i = this.lengthrow(sql);
		System.out.println(i);
		return i;
		
	}
 
}

