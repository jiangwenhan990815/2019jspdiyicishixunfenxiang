package com.Shopping.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Shopping.Server.videoloc;


public class videolocDaoImpl extends BaseDao implements videolocDao{
	ResultSet rs=null;
	Connection cnt=null;
	PreparedStatement ps=null;
	
	//查询全部信息
	@Override
	public ArrayList<videoloc> getList() {
		ArrayList<videoloc> array = new ArrayList<videoloc>();
		String sql="select * from videoloc";//查询表全部信息
		//初始化 数组 ，避免空指针
		Object[] params={};
		rs=this.ExecuteQuery(sql, params);
		
		//在控制台输出 rs的结果集
		try {
			while(rs.next()){
				videoloc us = new videoloc();
				us.setId(rs.getInt(1));
				us.setLoc(rs.getString(2));
				us.setType(rs.getString(3));
				us.setName(rs.getString(4));
				us.setAuthor(rs.getString(5));
				array.add(us);
			}
			return array;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			this.closeResource();
		}
		return array;
	}
 
	//增
	@Override
	public void add(int id,String loc,String type,String name,String author) {
		String sql="insert into videoloc(id,loc,type,name,author) values(?,?,?,?,?)";
		Object [] params={id,loc,type,name,author};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("添加成功");
		}
	}
 
	//删除
	@Override
	public void delete(String user) {
		String sql="delete from videoloc where user=?";
		Object [] params={user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("删除成功");
		}
	}
	
//修改
	@Override
	public void update(String pass,String email) {
		String sql="update videoloc set pass=? where email=?";
		Object [] params={pass,email};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("修改成功");
			
		}
	}
	public int length() {
		String sql = "SELECT * FROM videoloc;";
		int i = this.lengthrow(sql);
		System.out.println(i);
		return i;
		
	}
 
}