package com.Shopping.Dao;


import java.util.ArrayList;
import java.util.Date;
   //接口类
public interface NewsDao {
	//查询全部信息
	public ArrayList getList();
	
	//增
	public void add(String user,String pass,String way,String email,String phone,String img);
	//删
	public void delete(String user);
	//改
	public void update(String pwd,String user);
	
	public int length() ;
	
}

