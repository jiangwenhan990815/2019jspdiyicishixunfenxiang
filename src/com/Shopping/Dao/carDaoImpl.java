package com.Shopping.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Shopping.Server.carInf;

public class carDaoImpl extends BaseDao implements carDao{
	ResultSet rs=null;
	Connection cnt=null;
	PreparedStatement ps=null;
	
	//查询全部信息
	@Override
	public ArrayList<carInf> getList() {
		ArrayList<carInf> array = new ArrayList<carInf>();
		String sql="select * from shopCar";//查询表全部信息
		//初始化 数组 ，避免空指针
		Object[] params={};
		rs=this.ExecuteQuery(sql, params);
		
		//在控制台输出 rs的结果集
		try {
			while(rs.next()){
				carInf si = new carInf();
				si.setId(rs.getInt(1));
				si.setNum(rs.getInt(2));
				si.setPrice(rs.getDouble(3));
				si.setAllprice(rs.getDouble(4));
				si.setBuyer(rs.getString(5));
				si.setSeller(rs.getString(6));
				si.setLocal(rs.getString(7));
				array.add(si);
			}
			return array;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			this.closeResource();
		}
		return array;
	}
 
	//增
	@Override
	public void add(int id,int num,double price,double allprice,String buyer,String seller,String local) {
		String sql="insert into shopCar(id,num,price,allprice,buyer,seller,local) values(?,?,?,?,?,?,?)";
		Object [] params={id,num,price,allprice,buyer,seller,local};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("添加成功");
		}
	}
 
	//删除
	@Override
	public void delete(int id,String user) {
		String sql="delete from shopCar where id=? and buyer=?";
		Object [] params={id,user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("删除成功");
		}
	}
	
//修改
	@Override
	public void update(int num,double price,int id,String user) {
		String sql="update shopCar set num=?,allprice=?*? where id=? and buyer=?";
		Object [] params={num,num,price,id,user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("修改成功");
			
		}
	}
	public int length() {
		String sql = "SELECT * FROM shopping.shopCar;";
		int i = this.lengthrow(sql);
		System.out.println(i);
		return i;
		
	}
 
}
