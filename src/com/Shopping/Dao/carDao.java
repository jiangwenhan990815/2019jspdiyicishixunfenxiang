package com.Shopping.Dao;

import java.util.ArrayList;

public interface carDao {
	//查询全部信息
		public ArrayList getList();
		
		//增
		public void add(int id,int num,double price,double allprice,String buyer,String seller,String local);
		//删
		public void delete(int id,String user);
		//改
		public void update(int num,double price,int id,String user);
		
		public int length();//获取数据库的长度
}
