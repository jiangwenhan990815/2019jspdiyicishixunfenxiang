package com.Shopping.Dao;

import java.util.ArrayList;
import java.util.Date;
   //接口类
public interface videolocDao {
	//查询全部信息
	public ArrayList getList();
	
	//增
	public void add(int id,String loc,String type,String name,String author);
	//删
	public void delete(String user);
	//改
	public void update(String pwd,String user);//修改密码
	
	public int length();//获取数据库的长度
}