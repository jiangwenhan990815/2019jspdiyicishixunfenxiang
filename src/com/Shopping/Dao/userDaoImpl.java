package com.Shopping.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Shopping.Server.user;


public class userDaoImpl extends BaseDao implements userDao{
	ResultSet rs=null;
	Connection cnt=null;
	PreparedStatement ps=null;
	
	//查询全部信息
	@Override
	public ArrayList<user> getList() {
		ArrayList<user> array = new ArrayList<user>();
		String sql="select * from user";//查询表全部信息
		//初始化 数组 ，避免空指针
		Object[] params={};
		rs=this.ExecuteQuery(sql, params);
		
		//在控制台输出 rs的结果集
		try {
			while(rs.next()){
				user us = new user();
				us.setUser(rs.getString(1));
				us.setPass(rs.getString(2));
				us.setWay(rs.getString(3));
				us.setEmail(rs.getString(4));
				us.setImg(rs.getString(5));
				array.add(us);
			}
			return array;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			this.closeResource();
		}
		return array;
	}
 
	//增
	@Override
	public void add(String user,String pass,String way,String email,String img) {
		String sql="insert into user(name,pass,way,email,img) values(?,?,?,?,?)";
		Object [] params={user,pass,way,email,img};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("添加成功");
		}
	}
 
	//删除
	@Override
	public void delete(String user) {
		String sql="delete from user where user=?";
		Object [] params={user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("删除成功");
		}
	}
	
//修改
	@Override
	public void update(String pass,String user) {
		String sql="update user set pass=? where name=?";
		Object [] params={pass,user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("修改成功");
			
		}
	}
	public void updateimg(String user,String img) {
		String sql="update user set img=? where name=?";
		Object [] params={img,user};
		int i=this.executeUpdate(sql, params);
		if(i>0){
			System.out.println("修改成功");
			
		}
	}
	public int length() {
		String sql = "SELECT * FROM user;";
		int i = this.lengthrow(sql);
		System.out.println(i);
		return i;
		
	}
 
}



