package com.Shopping.Servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.Shopping.Dao.NewsDao;
import com.Shopping.Dao.NewsDaoImpl;
import com.Shopping.Dao.imgDao;
import com.Shopping.Dao.imgDaoImpl;
import com.Shopping.Dao.shopDao;
import com.Shopping.Dao.shopDaoImpl;


/**
 * Servlet implementation class shopAdd
 */
@WebServlet("/shopAdd")
public class shopAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public shopAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
    	response.setCharacterEncoding("utf-8");
    	response.setContentType("text/html; charset=UTF-8");
    	ServletContext application = this.getServletContext();
    	String user = (String)application.getAttribute("user");
    	shopDao nd = new shopDaoImpl();//获取Dao层对象，并获取数据库的指
    	imgDao imd = new imgDaoImpl();
    	
    	int id = nd.length()+1;//商品编号
    	String name = null;//商品名称
    	String type = null;//商品类型
    	String local = null;
    	double price = -1;
    	try {
    		boolean ifMultipart = ServletFileUpload.isMultipartContent(request);
    		if(ifMultipart) {
    			FileItemFactory factory = new DiskFileItemFactory();
    			ServletFileUpload upload = new ServletFileUpload(factory);
    			List<FileItem> items = upload.parseRequest(request);
    			Iterator<FileItem> iter = items.iterator();
    			while(iter.hasNext()) {
    				FileItem item = iter.next();
    				if(item.isFormField()) {
    					if(item.getFieldName().equals("name")) {
    						name = item.getString("utf-8");
    					}else if(item.getFieldName().equals("type")) {
    						type = item.getString("utf-8");
    					}else if(item.getFieldName().equals("price")) {
    						price = Double.parseDouble(item.getString("utf-8"));
    					}else {
    						local = item.getString("utf-8");
    					}
    				}
    				else {
    					String fileName = item.getName();
    					
    					String path = "/Users/jiangwenhan/eclipse-workspace/webShopping/WebContent/shopImg";
    					
    					File file = new File(path, fileName);
    					
    					item.write(file);
    					
    					if(item.getFieldName().equals("photo1")) {
    						imd.add(id, fileName, 1);//1表示前台展示
    					}else {
    						imd.add(id, fileName, 2);//2表示后台展示
    					}
    				}
    			}
    			nd.add(id, name, user, price, 1000, type,local);
    			PrintWriter out = response.getWriter();
    			out.write("true");
    			response.sendRedirect("http://localhost:8080/webShopping/userShow/index.jsp");
    		}
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
	}

}
