package com.Shopping.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Shopping.Dao.NewsDao;
import com.Shopping.Dao.NewsDaoImpl;
import com.Shopping.Dao.userDao;
import com.Shopping.Dao.userDaoImpl;
import com.Shopping.Server.user;
import com.Shopping.Server.userInf;

/**
 * Servlet implementation class Newuser
 */
@WebServlet("/Newuser")
public class Newuser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Newuser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
    	response.setCharacterEncoding("utf-8");
    	response.setContentType("text/html; charset=UTF-8");//对请求与响应客户端进行字符集处理
    	PrintWriter out = response.getWriter();
    	userDao nd = new userDaoImpl();//获取Dao层对象，并获取数据库的指
		ArrayList<user> userInf = nd.getList();
		int len = nd.length();
		int flag = 0;
    	String user = request.getParameter("user");
    	String pass = request.getParameter("pass");
    	String way = request.getParameter("way");
    	String email = request.getParameter("email");
    	for(int i = 0;i<len;i++) {
    		if(userInf.get(i).getUser().equals(user)) {
    			flag=1;
    			out.write("false");
    			return;
    		}
    	}
    	if(flag==0){
    		nd.add(user, pass, way, email,"zyx.png");//默认头像,不设置
    		out.write("true");
    		return;
    	}
	}

}
