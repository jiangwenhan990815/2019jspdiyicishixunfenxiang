package com.Shopping.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Shopping.Server.user;


/**
 * Servlet implementation class sendEmail
 */
@WebServlet("/sendEmail")
public class sendEmail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public sendEmail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		String[] zm = {"A","B","C","D","E","F","G","H","I","J","K","L","M","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9"};
		String yz = "";
		Random random = new Random();
		for(int i = 0;i<4;i++) {
			int j = random.nextInt(35);
			yz+=zm[j];
		}
		
		user us = new user();
		String username = new String(request.getParameter("user").getBytes("iso-8859-1"),"utf-8");
		String way = new String(request.getParameter("way").getBytes("iso-8859-1"),"utf-8");
		String pass = request.getParameter("pass");
		String repass = request.getParameter("repass");
		String email = request.getParameter("email");
		String yzm = request.getParameter("yzm");
		us.setUser(username);
		us.setEmail(email);
		session.setAttribute("user",username);
		session.setAttribute("pass",pass);
		session.setAttribute("repass",repass);
		session.setAttribute("email",email);
		session.setAttribute("way",way);
		session.setAttribute("yzm",yzm);
		
		emailutil sendMail=new emailutil();
		//获取邮箱验证
		String content="亲爱的"+username+",你的验证码为"+yz+",请查收？";
		try {
			sendMail.send(email,content);
			session.setAttribute("yz", yz);
			response.sendRedirect(request.getHeader("Referer")+"?yz="+yz);
//			response.getWriter().print("<scrpti>window.history.back(-1)</script>");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		selectEmailLink=SendMailUtil.SelectEmailLink(us.getEmail());
		
//		response.sendRedirect("../signIn.jsp");
		
		out.flush();
		out.close();
	}

}
