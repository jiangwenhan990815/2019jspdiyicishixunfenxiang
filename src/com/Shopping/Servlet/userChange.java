package com.Shopping.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Shopping.Dao.NewsDao;
import com.Shopping.Dao.NewsDaoImpl;
import com.Shopping.Dao.userDao;
import com.Shopping.Dao.userDaoImpl;
import com.Shopping.Server.userInf;

/**
 * Servlet implementation class userChange
 */
@WebServlet("/userChange")
public class userChange extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public userChange() {
        super();
    }
    
    protected void ifUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    //检测用户密码是否正确？
    	ServletContext application = this.getServletContext();
    	request.setCharacterEncoding("utf-8");
    	response.setCharacterEncoding("utf-8");
    	response.setContentType("text/html; charset=UTF-8");//对请求与响应客户端进行字符集处理
    	PrintWriter out=response.getWriter();
    	int check = 0;//验证用户的有效性
    	String user = request.getParameter("user");
    	String pass = request.getParameter("pass");//对上层用户以及参值进行获取
		userDao nd = new userDaoImpl();//获取Dao层对象，并获取数据库的指
		ArrayList<com.Shopping.Server.user> userInf = nd.getList();
		for(int i = 0;i<userInf.size();i++) {
			if(user.equals(userInf.get(i).getUser())&&pass.equals(userInf.get(i).getPass())) {
				if(userInf.get(i).getWay().equals("老师")) {
					out.write("teacher");
					application.setAttribute("user", user);
					application.setAttribute("way", "老师");
				}
				if(userInf.get(i).getWay().equals("学生")) {
					out.write("student");
					application.setAttribute("user", user);
					application.setAttribute("way", "同学	");
				}
				check=1;
				return;
			}
		}
		if(check == 0) {
			out.write("false");
		}
    }
    
    protected void newuser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    //注册一个新的用户名
    	request.setCharacterEncoding("utf-8");
    	response.setCharacterEncoding("utf-8");
    	response.setContentType("text/html; charset=UTF-8");//对请求与响应客户端进行字符集处理
    	PrintWriter out = response.getWriter();
    	NewsDao nd = new NewsDaoImpl();//获取Dao层对象，并获取数据库的指
		ArrayList<userInf> userInf = nd.getList();
    	String user = request.getParameter("user");
    	String pass = request.getParameter("pass");
    	String repass = request.getParameter("repass");
    	String way = request.getParameter("way");
    	String phone = request.getParameter("phone");
    	String email = request.getParameter("email");
    	String Eway = request.getParameter("Eway");
    	String allEmail = email+"@"+Eway+".com";
    	if(!(user.length()<2||user.length()>8)&&repass.length()>=6&&way!=null&&phone.length()==11&&email.length()>0&&Eway.length()>0){
    		for(int i = 0;i<userInf.size();i++) {
    			if(user.equals(userInf.get(i).getUser())) {
    				System.out.println(user+"  "+pass+"  "+repass+"  "+way);
    				out.write("error");//存在用户名，返回提示值该用户名已经存在
    				return;
    			}
    		}
    		nd.add(user, repass, way, allEmail, phone, "11");
    		System.out.println(user+"  "+pass+"  "+repass+"  "+way);
    		out.write("success");
    		return;
    	}
    	else {
    		out.write("warn");
    		return;
    	}
    }
    
    protected void exit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    //账号退出
    	ServletContext application = this.getServletContext();
    	application.setAttribute("user", null);
    	response.sendRedirect("http://localhost:8080/education/indexShow/index.jsp");
    }
    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		
		if(action == null) action="";
		else if(action.equals("if")) {
			ifUser(request, response);
		}
		else if(action.equals("new")) {
			newuser(request,response);
		}else if(action.equals("exit")) {
			exit(request,response);
		}
	}

}
