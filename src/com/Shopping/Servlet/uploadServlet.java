package com.Shopping.Servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;



/**
 * Servlet implementation class uploadServlet
 */
@WebServlet("/uploadServlet")
public class uploadServlet extends HttpServlet {
    private static final  int MAX_FILE_SIZE= 1024* 1024 * 500; //最大上传文件大小500MB
    //上传文件存储目录
    private static final String UPLOAD_DIRECTOR="upload";
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()YSS    
     */
    public uploadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        //response.getWriter().append("Served at: ").append(request.getContextPath());
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        //doGet(request, response);
        System.out.println("开始上传");
        try {
            //创建硬盘文件项目工厂类实例
            DiskFileItemFactory factory=new DiskFileItemFactory();
            //创建处理文件上传处理程序
            ServletFileUpload fileUpload=new ServletFileUpload(factory);
            //设置上传文件的最大限额为500M
            fileUpload.setSizeMax(MAX_FILE_SIZE);
            //将客户端的请求内容解析转化为FileItem集合
            List<FileItem> fileItems=fileUpload.parseRequest(new ServletRequestContext(request));
            //开始将文件项目FileItem存储到硬盘
          boolean isSuccessful=saveFileItemList(fileItems,getSavePath());
          if(isSuccessful)
          {
              request.setAttribute("result", "文件上传成功!");
          }
          else {
              request.setAttribute("result", "文件上传失败!");
        }


        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            request.setAttribute("result", "文件上传失败!");

        }
        finally {
            request.getRequestDispatcher("/upload.jsp").forward(request, response);;
        }

    }

    /**
     * 将文件项目集合存储的硬盘的savePath路径
     * @param fileItems
     * @param savePath
     * @return
     * @throws Exception
     */
    private boolean saveFileItemList(List<FileItem> fileItems,String savePath) throws Exception {

        try {
            for(FileItem item:fileItems)
            {
                System.out.println("文件名:"+item.getName());

                //设置文件的存储路径
                String fileSavePath=savePath+File.separator+item.getName();
                //实例化File类，用以写入存储上传的文件
                File  saveFile= new File(fileSavePath);

                System.out.println("文件存储路径:"+fileSavePath);

                item.write(saveFile);

            }

            return true;

        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }

    }

    /**
     * 获取上传文件存储的目录路径
     * @return
     */
    private String getSavePath() {
        String savePath="/Users/jiangwenhan/eclipse-workspace/jsp1/WebContent/image";
        System.out.println(savePath);
        File uploadDir= new File(savePath);

        if(!uploadDir.exists())
        {
            uploadDir.mkdir();
        }

        return savePath;

    }

}

