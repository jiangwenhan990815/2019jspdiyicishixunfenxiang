package com.Shopping.Servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.Shopping.Dao.videolocDao;
import com.Shopping.Dao.videolocDaoImpl;

/**
 * Servlet implementation class videoOn
 */
@WebServlet("/videoOn")
public class videoOn extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public videoOn() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
    	response.setCharacterEncoding("utf-8");
    	response.setContentType("text/html; charset=UTF-8");
    	ServletContext application = this.getServletContext();
    	
    	PrintWriter out = response.getWriter();
    	
    	String name = null;
    	String type = null;
    	
    	videolocDao vl = new videolocDaoImpl();
    	
    	try {
    		boolean ifMultipart = ServletFileUpload.isMultipartContent(request);
    		if(ifMultipart) {
    			FileItemFactory factory = new DiskFileItemFactory();
    			ServletFileUpload upload = new ServletFileUpload(factory);
    			List<FileItem> items = upload.parseRequest(request);
    			Iterator<FileItem> iter = items.iterator();
    			while(iter.hasNext()) {
    				FileItem item = iter.next();
    				if(item.isFormField()) {
    					if(item.getFieldName().equals("name")) {
    						name = item.getString("utf-8");
    						System.out.println(name+"   "+type);
    					}else if(item.getFieldName().equals("type")) {
    						type = item.getString("utf-8");
    						System.out.println(name+"   "+type);
    					}
    				}
    				else {
    					System.out.println("dadaad");
    					String path = null;
    					String fileName = item.getName();
    					if(type.equals("语文")) {
    						path = "/Users/jiangwenhan/eclipse-workspace/education/WebContent/ywvido";
    					}else if(type.equals("数学")) {
    						path = "/Users/jiangwenhan/eclipse-workspace/education/WebContent/sxvido";
    					}else {
    						path = "/Users/jiangwenhan/eclipse-workspace/education/WebContent/yyvido";
    					}
    					
    					File file = new File(path, fileName);
    					
    					item.write(file);
    					vl.add(vl.length()+1, fileName, type, name,(String)application.getAttribute("user"));
    					out.print("true");
    					response.sendRedirect("http://localhost:8080/education/indexShow/index.jsp");
    				}
    			}
    		}
    		else {
    			System.out.println("文件格式不对");
    		}
    	}catch(Exception e) {
    		e.printStackTrace();
    		System.out.println("文件上传出错!");
    	}
	}

}
