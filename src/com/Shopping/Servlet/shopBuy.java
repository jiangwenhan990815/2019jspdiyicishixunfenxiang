package com.Shopping.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Shopping.Dao.*;
import com.Shopping.Server.*;

/**
 * Servlet implementation class shopBuy
 */
@WebServlet("/shopBuy")
public class shopBuy extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public shopBuy() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		ServletContext application = this.getServletContext();
		
		int flag = 0;
		int i = Integer.parseInt(request.getParameter("id"));
		int num = Integer.parseInt(request.getParameter("num1"));
		shopDao sd = new shopDaoImpl();
		imgDao id = new imgDaoImpl();
		NewsDao nd = new NewsDaoImpl();
		carDao cd = new carDaoImpl();
		int sdlen = sd.length();
		int idlen = id.length();
		int ndlen = nd.length();
		int cdlen = cd.length();
		ArrayList<shopInf> siarray = sd.getList();
		ArrayList<imgInf> idarray = id.getList();
		ArrayList<userInf> ndarray = nd.getList();
		ArrayList<carInf> cdarray = cd.getList();
		double price = siarray.get(i-1).getPrice();
		String buyer = (String)application.getAttribute("user");
		String seller = siarray.get(i-1).getSeller();
		String local = siarray.get(i-1).getLocal();
		
		for(int j = 0;j<cdlen;j++) {
			String user = cdarray.get(j).getBuyer();
			int id1 = cdarray.get(j).getId();
			int num1 = cdarray.get(j).getNum();
			if(id1==i&&user.equals(buyer)) {
				cd.update(num1+num,price, id1, user);
				response.sendRedirect("http://localhost:8080/webShopping/shopShow/shopCar.jsp");
				return;
			}
		}
		if(flag==0) {
			cd.add(i, num, price, price*num, buyer, seller, local);
			response.sendRedirect("http://localhost:8080/webShopping/shopShow/shopCar.jsp");
			return;
		}
	}

}
