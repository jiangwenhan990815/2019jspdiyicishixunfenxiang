package com.Shopping.Servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.Shopping.Dao.userDao;
import com.Shopping.Dao.userDaoImpl;

/**
 * Servlet implementation class imageChange
 */
@WebServlet("/imageChange")
public class imageChange extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public imageChange() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext application = this.getServletContext();
    	String user = (String)application.getAttribute("user");
    	userDao ud = new userDaoImpl();
    	String fileName = null;
    	
    	try {
    		boolean ifMultipart = ServletFileUpload.isMultipartContent(request);
    		if(ifMultipart) {
    			FileItemFactory factory = new DiskFileItemFactory();
    			ServletFileUpload upload = new ServletFileUpload(factory);
    			List<FileItem> items = upload.parseRequest(request);
    			Iterator<FileItem> iter = items.iterator();
    			while(iter.hasNext()) {
    				FileItem item = iter.next();
    				if(item.isFormField()) {
    					
    				}
    				else {
    					fileName = item.getName();
    					
    					String path = "/Users/jiangwenhan/eclipse-workspace/education/WebContent/image";
    					
    					File file = new File(path, fileName);
    					
    					item.write(file);
    					

    	    			PrintWriter out = response.getWriter();
    	    			out.write("true");
    					
    					ud.updateimg(user, fileName);
    					
    				}
    			}
    		}
    	}catch(FileUploadException e) {
    		e.printStackTrace();
    	} catch (Exception e) {
			// TODO Auto-generated catch block
    		
    		System.out.println("dada");
    		
    		ud.updateimg(user, fileName);
    		
    		response.sendRedirect("http://localhost:8080/education/indexShow/index.jsp");
    		
			e.printStackTrace();
		}
    	
    	
	}

}
