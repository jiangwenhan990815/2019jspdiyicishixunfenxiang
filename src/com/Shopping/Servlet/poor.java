package com.Shopping.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Shopping.Dao.poorDao;
import com.Shopping.Dao.poorDaoImpl;
import com.sun.net.httpserver.HttpContext;



/**
 * Servlet implementation class poor
 */
@WebServlet("/poor")
public class poor extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public poor() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    protected void poor(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    //发表评论专区
    	ServletContext application = this.getServletContext();
    	int id = Integer.parseInt(request.getParameter("id"));
    	String name = (String)application.getAttribute("user");
    	String text = request.getParameter("text");
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
    	
    	poorDao pd = new poorDaoImpl();
    	pd.add(id, name, text, (String)df.format(new Date()));
    	PrintWriter out = response.getWriter();
    	out.write("true");
    	
    }
    
    
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getParameter("action");
		
		if(action==null)action="";
		else if(action.equals("poor")){
			poor(request, response);
		}
		
		
	}

}
