package com.Shopping.Servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class shopmain
 */
@WebServlet("/shopmain")
public class shopmain extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public shopmain() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void exit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    //退出
    	ServletContext application = this.getServletContext();
    	application.setAttribute("user",null);
		response.sendRedirect("http://localhost:8080/webShopping/shopShow/main.jsp");
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if(action == null) action="";
		if(action.equals("exit")) {
			exit(request,response);
		}
	}

}
