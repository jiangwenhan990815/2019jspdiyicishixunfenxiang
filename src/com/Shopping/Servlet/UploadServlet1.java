package com.Shopping.Servlet;


import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;




/**
 * Servlet implementation class UploadServlet1
 */
@WebServlet("/UploadServlet1")
public class UploadServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String adjunctname;
		String fileDir = request.getRealPath("upload/");
		String message = "文件上传成功";
		String address = "";
		if(ServletFileUpload.isMultipartContent(request)) {
			DiskFileItemFactory factory = new DiskFileItemFactory();
			factory.setSizeThreshold(20*1024);
			factory.setRepository(factory.getRepository());
			ServletFileUpload upload = new ServletFileUpload();
			int size = 2*1024*1024;
			List formlists = null;
			try {
				formlists = upload.parseRequest(request);
			}catch(FileUploadException e) {
				e.printStackTrace();
			}
			Iterator iter = formlists.iterator();
			while(iter.hasNext()) {
				FileItem formitem = (FileItem)iter.next();
				if(!formitem.isFormField()) {
					String name = formitem.getName();
					if(formitem.getSize()>size) {
						message="你上传的文件太大，请选择不超过2M的文件！";
						break;
					}
					String adjunctsize = new Long(formitem.getSize()).toString();
					if((name == null)||(name.equals(""))&&(adjunctsize.equals("0")))
						continue;
					adjunctname = name.substring(name.lastIndexOf("\\")+1,name.length());
					address = fileDir+"\\"+adjunctname;
					File saveFile = new File(address);
					try {
						formitem.write(saveFile);
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		request.setAttribute("result", message);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("upload.jsp");
		requestDispatcher.forward(request, response);
	}

}
