package com.Shopping.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Shopping.Dao.*;
import com.Shopping.Server.*;

/**
 * Servlet implementation class shopCar
 */
@WebServlet("/shopCar")
public class shopCar extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public shopCar() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void low(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//商品减少事件
    	shopDao sd = new shopDaoImpl();
    	imgDao id = new imgDaoImpl();
    	NewsDao nd = new NewsDaoImpl();
    	carDao cd = new carDaoImpl();
    	order1Dao od1 = new order1DaoImpl();
    	order2Dao od2 = new order2DaoImpl();
    	int sdlen = sd.length();
    	int idlen = id.length();
    	int ndlen = nd.length();
    	int cdlen = cd.length();
    	int od1len = od1.length();
    	int od2len = od2.length();
    	ArrayList<shopInf> siarray = sd.getList();
    	ArrayList<imgInf> idarray = id.getList();
    	ArrayList<userInf> ndarray = nd.getList();
    	ArrayList<carInf> cdarray = cd.getList();
    	ArrayList<shoporder1> od1array = od1.getList();
    	ArrayList<shoporder2> od2array = od2.getList();
    	request.setCharacterEncoding("utf-8");
    	response.setCharacterEncoding("utf-8");
    	response.setContentType("text/html; charset=UTF-8");
    	
    	String user = new String(request.getParameter("user").getBytes("iso-8859-1"),"utf-8");
    	int id2 = Integer.parseInt( request.getParameter("id"));
    	
    	System.out.println(user);
    	
    	for(int i = 0;i<cdlen;i++) {
    		String buyer = cdarray.get(i).getBuyer();
    		int id1 = cdarray.get(i).getId();
    		int num = cdarray.get(i).getNum();
    		double price = cdarray.get(i).getPrice();
    		if(num==0) {
    			response.sendRedirect("http://localhost:8080/webShopping/shopShow/shopCar.jsp");
    		}
    		if(user.equals(buyer)&&id2==id1) {
    			cd.update(num-1, price, id2, user);
    			response.sendRedirect("http://localhost:8080/webShopping/shopShow/shopCar.jsp");
    		}
    	}
	}
    
    
    protected void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//商品增加事件
    	shopDao sd = new shopDaoImpl();
    	imgDao id = new imgDaoImpl();
    	NewsDao nd = new NewsDaoImpl();
    	carDao cd = new carDaoImpl();
    	order1Dao od1 = new order1DaoImpl();
    	order2Dao od2 = new order2DaoImpl();
    	int sdlen = sd.length();
    	int idlen = id.length();
    	int ndlen = nd.length();
    	int cdlen = cd.length();
    	int od1len = od1.length();
    	int od2len = od2.length();
    	ArrayList<shopInf> siarray = sd.getList();
    	ArrayList<imgInf> idarray = id.getList();
    	ArrayList<userInf> ndarray = nd.getList();
    	ArrayList<carInf> cdarray = cd.getList();
    	ArrayList<shoporder1> od1array = od1.getList();
    	ArrayList<shoporder2> od2array = od2.getList();
    	request.setCharacterEncoding("utf-8");
    	response.setCharacterEncoding("utf-8");
    	response.setContentType("text/html; charset=UTF-8");
    	
    	String user = new String(request.getParameter("user").getBytes("iso-8859-1"),"utf-8");
    	int id2 = Integer.parseInt( request.getParameter("id"));
    	
    	System.out.println(user);
    	
    	for(int i = 0;i<cdlen;i++) {
    		String buyer = cdarray.get(i).getBuyer();
    		int id1 = cdarray.get(i).getId();
    		int num = cdarray.get(i).getNum();
    		double price = cdarray.get(i).getPrice();
    		if(num==0) {
    			response.sendRedirect("http://localhost:8080/webShopping/shopShow/shopCar.jsp");
    		}
    		if(user.equals(buyer)&&id2==id1) {
    			cd.update(num+1, price, id2, user);
    			response.sendRedirect("http://localhost:8080/webShopping/shopShow/shopCar.jsp");
    		}
    	}
	}
    
    
    protected void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//商品增加事件
    	shopDao sd = new shopDaoImpl();
    	imgDao id = new imgDaoImpl();
    	NewsDao nd = new NewsDaoImpl();
    	carDao cd = new carDaoImpl();
    	order1Dao od1 = new order1DaoImpl();
    	order2Dao od2 = new order2DaoImpl();
    	int sdlen = sd.length();
    	int idlen = id.length();
    	int ndlen = nd.length();
    	int cdlen = cd.length();
    	int od1len = od1.length();
    	int od2len = od2.length();
    	ArrayList<shopInf> siarray = sd.getList();
    	ArrayList<imgInf> idarray = id.getList();
    	ArrayList<userInf> ndarray = nd.getList();
    	ArrayList<carInf> cdarray = cd.getList();
    	ArrayList<shoporder1> od1array = od1.getList();
    	ArrayList<shoporder2> od2array = od2.getList();
    	request.setCharacterEncoding("utf-8");
    	response.setCharacterEncoding("utf-8");
    	response.setContentType("text/html; charset=UTF-8");
    	
    	String user = new String(request.getParameter("user").getBytes("iso-8859-1"),"utf-8");
    	int id2 = Integer.parseInt( request.getParameter("id"));
    	
    	System.out.println(user);
    	
    	for(int i = 0;i<cdlen;i++) {
    		String buyer = cdarray.get(i).getBuyer();
    		int id1 = cdarray.get(i).getId();
    		if(user.equals(buyer)&&id2==id1) {
    			cd.delete(id2, user);
    			response.sendRedirect("http://localhost:8080/webShopping/shopShow/shopCar.jsp");
    		}
    	}
	}
    
    
    protected void buy(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//商品增加事件
    	shopDao sd = new shopDaoImpl();
    	imgDao id = new imgDaoImpl();
    	NewsDao nd = new NewsDaoImpl();
    	carDao cd = new carDaoImpl();
    	order1Dao od1 = new order1DaoImpl();
    	order2Dao od2 = new order2DaoImpl();
    	int sdlen = sd.length();
    	int idlen = id.length();
    	int ndlen = nd.length();
    	int cdlen = cd.length();
    	int od1len = od1.length();
    	int od2len = od2.length();
    	ArrayList<shopInf> siarray = sd.getList();
    	ArrayList<imgInf> idarray = id.getList();
    	ArrayList<userInf> ndarray = nd.getList();
    	ArrayList<carInf> cdarray = cd.getList();
    	ArrayList<shoporder1> od1array = od1.getList();
    	ArrayList<shoporder2> od2array = od2.getList();
    	request.setCharacterEncoding("utf-8");
    	response.setCharacterEncoding("utf-8");
    	response.setContentType("text/html; charset=UTF-8");
    	
    	String user = new String(request.getParameter("user").getBytes("iso-8859-1"),"utf-8");
    	String getid[] = request.getParameterValues("choose");
    	if(getid!=null) {
	    	int len = getid.length;
	    	String name=null;
	    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	    	
	    	int id2[] = new int[len];
	    	for(int i = 0;i<len;i++) {
	    		id2[i] = Integer.parseInt(getid[i]);
	    		for(int j = 0;j<sdlen;j++) {
	    			if(id2[i]==siarray.get(j).getId()) {
	    				name=siarray.get(j).getName();
	    			}
	    		}
	    		for(int j = 0;j<cdlen;j++) {
	    			if(id2[i]==cdarray.get(j).getId()&&user.equals(cdarray.get(j).getBuyer())) {//购物车表符合条件时
	    				String seller = cdarray.get(j).getSeller();
	    				int num = cdarray.get(j).getNum();
	    				double price = cdarray.get(j).getPrice();
	    				for(int k = 0;k<idlen;k++) {
	    					if(idarray.get(k).getId()==id2[i]&&idarray.get(k).getIfShow()==1) {
	    						String img = idarray.get(k).getImg();
	    						cd.delete(id2[i], user);
	    						od2.add(od1len+1, id2[i], name, img, price, num, seller);
	    					}
	    				}
	    			}
	    		}
	    	}
		    	od1.add(od1len+1, user, df.format(new Date()));
		    	response.sendRedirect("http://localhost:8080/webShopping/shopShow/orderShow.jsp");
				return;
    	}
    	else {
    		PrintWriter out = response.getWriter();
    		out.print("<script>alert('购物车啥都没有')</script>");
    		response.sendRedirect("http://localhost:8080/webShopping/shopShow/main.jsp");
    		return;
    	}
    	
//    	System.out.println("num"+getid.length);
//    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
//		 System.out.println(df.format(new Date()));
	}
    
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");

    	ServletContext application = this.getServletContext();
		
		
		if(action == null) action="";
		else if(action.equals("low")) {
			low(request,response);
		}
		else if(action.equals("add")) {
			add(request,response);
		}
		else if(action.equals("delete")) {
			delete(request,response);
		}
		else if(action.equals("buy")) {
			buy(request,response);
		}
	}

}
