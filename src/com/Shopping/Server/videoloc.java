package com.Shopping.Server;

public class videoloc {
	private int id;//视频的编号
	private String loc;//视频地址
	private String type;//视频的分类
	private String name;//视频的名称
	private String author;//视频上传者，学生也可以上传，但是要经过审核，教师可以随意上传，学生可以举报
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String get() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
}	
