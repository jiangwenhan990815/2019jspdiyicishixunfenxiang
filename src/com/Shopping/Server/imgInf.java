package com.Shopping.Server;

public class imgInf {
	private int id;//商品编号，不单一
	private String img;//图片的名称
	private int ifShow;//是否前台展示
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public int getIfShow() {
		return ifShow;
	}
	public void setIfShow(int ifShow) {
		this.ifShow = ifShow;
	}
	
}
