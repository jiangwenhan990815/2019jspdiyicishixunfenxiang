package com.Shopping.Server;

public class userInf {//用户信息封装类
	private String user;//用户名
	private String pwd;//密码
	private String way;//用户登录方式
	private String email;//邮箱地址
	private String phone;//手机
	private String img;//用户头像照片
	public void userInf() {
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getUser() {
		return user;
	}
	public void setPass(String pwd) {
		this.pwd = pwd;
	}
	public String getPass() {
		return pwd;
	}
	public void setWay(String way) {
		this.way = way;
	}
	public String getWay() {
		return way;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPhone() {
		return phone;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getImg() {
		return img;
	}
}
