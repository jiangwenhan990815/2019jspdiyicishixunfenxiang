<%@page import="com.Shopping.Server.collect"%>
<%@page import="com.Shopping.Dao.collectDaoImpl"%>
<%@page import="com.Shopping.Dao.collectDao"%>
<%@page import="com.Shopping.Server.user"%>
<%@page import="com.Shopping.Dao.userDaoImpl"%>
<%@page import="com.Shopping.Dao.userDao"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.Shopping.Dao.videolocDao"%>
<%@page import="com.Shopping.Dao.videolocDaoImpl"%>
<%@page import="com.Shopping.Server.videoloc"%>
    <%
    	String user = (String)application.getAttribute("user");
    	String way = (String)application.getAttribute("way");
    	videolocDao vl = new videolocDaoImpl();
    	ArrayList<videoloc> vlarray = vl.getList();
    	userDao ud = new userDaoImpl();
    	ArrayList<user> udarray = ud.getList();
    	collectDao cd = new collectDaoImpl();
    	ArrayList<collect> cdarray = cd.getList();
    	user ituser = new user();
    	for(int i = 0;i<ud.length();i++){
    		if(udarray.get(i).getUser().equals(user)){
    			ituser = udarray.get(i);
    		}
    	}
    %>
<!doctype html>
<html>
    <head>
    	<%
    		if(user==null){
    	%>
    	<title>致伊教育欢迎你!</title>
    	<%
    		}else{
    	%>
    	<title><%=user %><%=way %>的主页</title>
    	<%
    		}
    	%>
        <meta  charset = "utf-8"/>
        <meta name = "viewport" content = "width-device=width,initial = 1.0"/>
        <!--bootstrap.css -->        
        <link  href = "../css/bootstrap.css" rel = "stylesheet" type = "text/css"/>
        <!-- bootstrap.js -->        
        <script src = "../js/jquery-1.11.3.js"> </script>
        <!-- bootstrap.js -->
            <script src = "../js/bootstrap.js"></script>
            <link href="../css/style1.css" rel="stylesheet">
            <style>
        body,
        h1, h2, h3, h4, h5, h6, hr, p, blockquote, dl, dt, dd, ul, ol, li, pre, form, fieldset, legend, button, input, textarea, th, td {
            margin: 0;
            padding: 0;
        }
 
        body, button, input, select, textarea {
            font: 12px/1.5 tahoma, arial, \5b8b\4f53;
        }
 
        h1, h2, h3, h4, h5, h6 {
            font-size: 100%;
        }
 
        address, cite, dfn, em, var {
            font-style: normal;
        }
 
        code, kbd, pre, samp {
            font-family: couriernew, courier, monospace;
        }
 
        small {
            font-size: 12px;
        }
 
        ul, ol {
            list-style: none;
        }
 
        a {
            text-decoration: none;
        }
 
        a:hover {
            text-decoration: underline;
        }
 
        sup {
            vertical-align: text-top;
        }
 
        sub {
            vertical-align: text-bottom;
        }
 
        legend {
            color: #000;
        }
 
        fieldset, img {
            border: 0;
        }
 
        button, input, select, textarea {
            font-size: 100%;
        }
 
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }
 
        body, html {
            background: #EAEEF2;
            width: 100%;
            height: 100%;
        }
 
        .container-fluid {
            background: aquamarine;
            height: 100%;
        }
 
        .logo {
            background: #354144;
            color: black;
            font-size: 40px;
            padding-left: 20px;
            height: 60px;
            min-height: 60px;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 100;
            width: 100%;
        }
 
        .nav > li > a:hover {
            color: #262626;
            text-decoration: none;
            background-color: #354144;
        }
 
        .open > a {
            background-color: #354144 !important;
        }
 
        .right {
            float: right;
        }
 
        .grid:hover {
            background: #efefef;
        }
 
    </style>
		<script>
			$(function () {
			$(".dropdown").mouseover(function () {
				$(this).addClass("open");
			});

			$(".dropdown").mouseleave(function () {
				$(this).removeClass("open");
			})
		})
		</script>



    </head>
    <body>
    <div class="logo">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10" style="background: #354144;color: white">致伊教育</div>
             <%
    			if(user==null){ 
		    %>
		     <div class="col-lg-2" style="background: #354144">
                <div class="right">
                	<a href="../userShow/signIn.jsp">登录</a>
                </div>
             </div>
		    <%
	    		}else{
		    %>
            <div class="col-lg-2" style="background: #354144">
                <div class="right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle"
                               data-toggle="dropdown"
                               style="height: 60px">
                                <img alt="" class="img-circle" src="../image/<%=ituser.getImg() %>" width="38px" height="38px"/>
                                <span style="color: #FFFFFF;font-size: 15px">
                                <i><%=user %></i>
                            </span>
                            </a>
                            <div class="dropdown-menu pull-right"
                                 style="background: #FFFFFF;width: 320px;overflow: hidden">
                                <div style="margin-top: 16px;border-bottom: 1px solid #eeeeee">
                                    <div style="text-align: center">
                                        <img class="img-circle" src="../image/<%=ituser.getImg() %>zyx.png"
                                             style="width: 38px;height: 38px;"/>
                                    </div>
                                    <div style="color: #323534;text-align: center;line-height: 36px;font-size: 15px">
                                        <span><%=user %></span>
                                    </div>
                                </div>
 								<a href="index.jsp">
                                <div class="row" style="margin-left: 15px;margin-right: 15px;margin-top: 10px">
                                    <div class="col-md-4 text-center grid">
                                        <i class="fa fa-user" style="font-size: 25px;line-height: 45px;"></i>
                                        <p style="padding: 0px;margin-top: 6px;margin-bottom: 10px;font-size: 12px">
                                            首页</p>
                                    </div>
                                    </a>
                                    <a href="../videoShow/newVideo.jsp">
                                    <div class="col-md-4 text-center grid">
                                        <i class="fa fa-gear" style="font-size: 25px;line-height: 45px;"></i>
                                        <p style="padding: 0px;margin-top: 6px;margin-bottom: 10px;font-size: 12px">
                                           视频上传</p>
                                    </div>
                                    </a>
                                    <div class="col-md-4 text-center grid">
                                        <i class="fa fa-key" style="font-size: 25px;line-height: 45px;"></i>
                                        <p style="padding: 0px;margin-top: 6px;margin-bottom: 10px;font-size: 12px">
                                            密码修改</p>
                                    </div>
                                </div>
 
                                <div class="row" style="margin-left: 15px;margin-right: 15px;margin-top: 10px">
                                    <div class="col-md-4 text-center grid">
                                        <i class="fa fa-user-circle" style="font-size: 25px;line-height: 45px;"></i>
                                        <p style="padding: 0px;margin-top: 6px;margin-bottom: 10px;font-size: 12px">
                                            修改头像</p>
                                    </div>
                                    <div class="col-md-4 text-center grid">
                                        <i class="fa fa-comments" style="font-size: 25px;line-height: 45px;"></i>
                                        <p style="padding: 0px;margin-top: 6px;margin-bottom: 10px;font-size: 12px">
                                            消息</p>
                                    </div>
                                    <div class="col-md-4 text-center grid">
                                        <i class="fa fa-heart-o" style="font-size: 25px;line-height: 45px;"></i>
                                        <p style="padding: 0px;margin-top: 6px;margin-bottom: 10px;font-size: 12px">
                                            管理请求</p>
                                    </div>
                                </div>
 
 							<a href="http://localhost:8080/education/userChange?action=exit">
                                <div class="row" style="margin-top: 20px">
                                    <div class="text-center"
                                         style="padding: 15px;margin: 0px;background: #f6f5f5;color: #323534;">
                                        <i class="fa fa-sign-out"></i> 退出账号
                                    </div>
                                </div>
                             </a>
                            </div>
                        </li>
                    </ul>
                </div>
 
            </div>
            <%
	    		}
            %>
        </div>
    </div>
 
</div>
<div class="container" style="top: 120px;position: absolute">
	<div class="row">
		<div class="col-md-12 col-xs-12 col-sm-12" style="font-size: 36px">我的信息</div>
 	</div>
 	<%
 		for(int i = 0;i<ud.length();i++){
 			if(udarray.get(i).getUser().equals(user)){
 	%>
		<div class="col-md-4 col-xs-12 col-sm-6"><img alt="" class="img-circle" src="../image/<%=udarray.get(i).getImg() %>" width="38px" height="38px"/></div>
		<div class="col-md-8 col-xs-12 col-sm-6" style="font-size: 24px">邮箱:<%=udarray.get(i).getEmail() %></div>
	<%
 			}
 		}
	%>
	<div class="row">
		<div class="col-md-12 col-xs-12 col-sm-12" style="font-size: 36px">同学关系:</div>
 	</div>
	<%
		int flag1 = 0;
		for(int i = 0;i<cd.length();i++){
			if((cdarray.get(i).getName1().equals(user)||cdarray.get(i).getName1().equals(user))&&cdarray.get(i).getRelation().equals("同学")){	
		
	%>
	<div class="col-md-1 col-xs-1 col-sm-1"><img alt="" class="img-circle" src="../image/<%=udarray.get(i).getImg() %>" width="38px" height="38px"/></div>
	<div class="col-md-5 col-xs-5 col-sm-5" style="font-size: 24px">姓名:<%=udarray.get(i).getUser() %></div>
 	<%
 				flag1=1;
			}
		}
 	%> 
 	<%
 		if(flag1 == 0){
 	%>
 	<div class="row">
		<div class="col-md-12 col-xs-12 col-sm-12" style="font-size: 36px">你还没有同学加进来，快去添加吧！</div>
 	</div>
 	<%
 		}
 	%>
 	<div class="row">
		<div class="col-md-12 col-xs-12 col-sm-12" style="font-size: 36px">师生关系:</div>
 	</div>
	<%
		int flag2 = 0;
		for(int i = 0;i<cd.length();i++){
			if((cdarray.get(i).getName1().equals(user)||cdarray.get(i).getName1().equals(user))&&cdarray.get(i).getRelation().equals("师生")){	
		
	%>
	<div class="col-md-1 col-xs-1 col-sm-1"><img alt="" class="img-circle" src="../image/<%=udarray.get(i).getImg() %>" width="38px" height="38px"/></div>
	<div class="col-md-5 col-xs-5 col-sm-5" style="font-size: 24px">姓名:<%=udarray.get(i).getUser() %></div>
 	<%
 				flag2=1;
			}
		}
 	%> 
 	<%
 		if(flag2 == 0){
 	%>
 	<div class="row">
		<div class="col-md-12 col-xs-12 col-sm-12" style="font-size: 36px">你还没有师生关系加进来，快去添加吧！</div>
 	</div>
 	<%
 		}
 	%>
 	<div class="row">
		<div class="col-md-12 col-xs-12 col-sm-12" style="font-size: 36px">好友关系:</div>
 	</div>
	<%
		int flag3 = 0;
		for(int i = 0;i<cd.length();i++){
			if((cdarray.get(i).getName1().equals(user)||cdarray.get(i).getName1().equals(user))&&cdarray.get(i).getRelation().equals("好友")){	
		
	%>
	<div class="col-md-1 col-xs-1 col-sm-1"><img alt="" class="img-circle" src="../image/<%=udarray.get(i).getImg() %>" width="38px" height="38px"/></div>
	<div class="col-md-5 col-xs-5 col-sm-5" style="font-size: 24px">姓名:<%=udarray.get(i).getUser() %></div>
 	<%
 				flag3=1;
			}
		}
 	%> 
 	<%
 		if(flag3 == 0){
 	%>
 	<div class="row">
		<div class="col-md-12 col-xs-12 col-sm-12" style="font-size: 36px">你还没有好友加进来，快去添加吧！</div>
 	</div>
 	<%
 		}
 	%>
 	<div class="row">
		<div class="col-md-12 col-xs-12 col-sm-12" style="font-size: 36px">可能认识的人:</div>
 	</div>
 	<%
 		for(int i = 0;i<ud.length();i++){
 			if(!udarray.get(i).getUser().equals(user)){
 	%>
 		<div class="col-md-1 col-xs-1 col-sm-1"><img alt="" class="img-circle" src="../image/<%=udarray.get(i).getImg() %>" width="38px" height="38px"/></div>
		<div class="col-md-5 col-xs-5 col-sm-5" style="font-size: 24px">姓名:<%=udarray.get(i).getUser() %></div>
		<div class="col-md-2 col-xs-2 col-sm-2"><button style="height: 30px;width:100%;background-color: springgreen" id="friend">好友</button></div>
		<div class="col-md-2 col-xs-2 col-sm-2"><button style="height: 30px;width:100%;background-color: springgreen" id="ts">师生</button></div>
		<div class="col-md-2 col-xs-2 col-sm-2"><button style="height: 30px;width:100%;background-color: springgreen" id="class">同学</button></div>
 	<%
 			}
 		}
 	%>
</div>


    </body>
</html>
