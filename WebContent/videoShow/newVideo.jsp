<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
<html  lang="en">

    <head>
        <!-- meta data -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <!--font-family-->
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&amp;subset=devanagari,latin-ext" rel="stylesheet">
        
        <!-- title of site -->
        <title>致伊平台视频上传</title>

        <!-- For favicon png -->
		<link rel="shortcut icon" type="image/icon" href="assets/logo/favicon.png"/>
       
        <!--font-awesome.min.css-->
        <link rel="stylesheet" href="../css/font-awesome.min.css">
		
		<!--animate.css-->
        <link rel="stylesheet" href="../css/animate.css">
		
        <!--bootstrap.min.css-->
        <link rel="stylesheet" href="../css/bootstrap.min.css">
		
		<!-- bootsnav -->
		<link rel="stylesheet" href="../css/bootsnav.css" >	
        
        <!--style.css-->
        <link rel="stylesheet" href="../css/style.css">
        
        <!--responsive.css-->
        <link rel="stylesheet" href="../css/responsive.css">
        <script src="../js/jquery-1.9.1.js"></script>
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		
        <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
	<!-- <script type="text/javascript">
	$(document).ready(function(){
		$("#login").click(function(){
			var use = $("#user").val();
			var pass = $("#password").val();
			$.post(
					"http://localhost:8080/education/userChange",
					{"action":"if","user":use,"pass":pass},
					function(result){
						if(result == "student"){
							alert("你是个学生");
							window.location.href="http://localhost:8080/education/indexShow/index.jsp";
						}
						else if(result == "teacher"){
							alert("你是个老师");
							window.location.href="http://localhost:8080/education/indexShow/index.jsp";
						}else{
							alert("密码或账号错误");
						}
					}
				); 
		});
	}
	);
	</script> -->
    </head>
	
	<body>
		<!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
		
		<!-- signin end -->
		<section class="signin popup-in">
			<div class="container">
				<div class="sign-content popup-in-content">
					<div class="popup-in-txt">
						<h2>致伊教育平台</h2>

						<div class="row">
							<div class="col-sm-12">
								<div class="signin-form">
									<form action="http://localhost:8080/education/videoOn" method="post" enctype="multipart/form-data">
										<div class="form-group">
										    <label for="name">视频名称:</label>
										    <input type="text" class="form-control" id="name" name="name" placeholder="请输入名称">
										</div><!--/.form-group -->
										<div class="form-group">
											<label for="type">视频分类:</label>
											<input type="radio" name="type" value="语文">语文
											<input type="radio" name="type" value="数学">数学
											<input type="radio" name="type" value="英语">英语
										</div><!--/.form-group -->
										<div class="form-group">
											<input type="file" name="video">
										</div><!--/.form-group -->
										<div class="col-sm-12">
											<div class="signin-footer">
												<input type="submit" id="login" value="上传" class="btn signin_btn signin_btn_two">
											</div><!--/.signin-footer -->
										</div><!--/.col -->
									</form><!--/form -->
								</div><!--/.signin-form -->
							</div><!--/.col -->
						</div><!--/.row -->

						<div class="row">
						</div><!--/.row -->
					</div><!-- .popup-in-txt -->
				</div><!--/.sign-content -->
			</div><!--/.container -->
		</section><!--/.signin -->
		
		<!-- signin end -->

		<!--footer copyright start -->
		<footer class="footer-copyright">
			<div id="scroll-Top">
				<i class="fa fa-angle-double-up return-to-top" id="scroll-top" data-toggle="tooltip" data-placement="top" title="" data-original-title="Back to Top" aria-hidden="true"></i>
			</div><!--/.scroll-Top-->

		</footer><!--/.hm-footer-copyright-->
		<!--footer copyright  end -->


		 <!-- Include all js compiled plugins (below), or include individual files as needed -->

		<script src="../js/jquery.js"></script>
        
        <!--modernizr.min.js-->
        <script src="../js/modernizr.min.js"></script>
		
		<!--bootstrap.min.js-->
        <script src="../js/bootstrap.min.js"></script>
		
		<!-- bootsnav js -->
		<script src="../js/bootsnav.js"></script>
		
		<!-- jquery.sticky.js -->
		<script src="../js/jquery.sticky.js"></script>
		<script src="../js/jquery.easing.min.js"></script>
		
        
        <!--Custom JS-->
        <script src="../js/custom.js"></script>

    </body>
	
</html>