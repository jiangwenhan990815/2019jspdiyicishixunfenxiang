<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
 <!-- meta data -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <!--font-family-->
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&amp;subset=devanagari,latin-ext" rel="stylesheet">
        
        <!-- title of site -->
        <title>致伊账号注册</title>

        <!-- For favicon png -->
		<link rel="shortcut icon" type="image/icon" href="assets/logo/favicon.png"/>
       
        <!--font-awesome.min.css-->
        <link rel="stylesheet" href="../css/font-awesome.min.css">
		
		<!--animate.css-->
        <link rel="stylesheet" href="../css/animate.css">
		
        <!--bootstrap.min.css-->
        <link rel="stylesheet" href="../css/bootstrap.min.css">
		
		<!-- bootsnav -->
		<link rel="stylesheet" href="../css/bootsnav.css" >	
        
        <!--style.css-->
        <link rel="stylesheet" href="../css/style.css">
        
        <!--responsive.css-->
        <link rel="stylesheet" href="../css/responsive.css">
        
		<script src="../js/jquery-1.9.1.js"></script>
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		
        <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
		<script>
		$(document).ready(function(){
			$("#login").click(function(){
				var use = $("#user").val();
				var way = $('input:radio[name="way"]:checked').val();
				var pass = $("#password").val();
				var repass = $("#repass").val();
				var email = $("#email").val();
				var yz = $("#yzm").val();
				if((use.length>2&&use.length<8)&&way!=null&&pass==repass&&yz=="<%=session.getAttribute("yz")%>"){
					alert(pass);
					$.post(
						"http://localhost:8080/education/Newuser",
						{"user":use,"pass":pass,"way":way,"email":email},
						function(result){
							if(result == "false"){
								alert("用户名已经被注册！");
							}
							else{
								alert("注册成功！快去登录一下看看吧?");
							}
						}
					); 
				}
				else{
					alert("输入格式有问题！");
				}
			});
		}
		);
	</script>
</head>
<body>
	<!-- signin end -->
		<section class="signin signup popup-in pop-up">
			<div class="container">

				<div class="sign-content popup-in-content">
					<div class="popup-in-txt">
						<h2>致伊账号注册</h2>

						<div class="signin-form">
							<div class=" ">
								<div class=" ">
									<form action="http://localhost:8080/education/sendEmail">
										<div class="form-group">
										    <label for="user">用户名:</label>
										    <%
										    	if(session.getAttribute("user")!=null){
										    %>
										    <input type="text" class="form-control" id="user" name="user" placeholder="请输入注册用户名!" value="<%=session.getAttribute("user")%>">
										    <%
										    	}else{
										    %>
										    <input type="text" class="form-control" id="user" name="user" placeholder="请输入注册用户名!" >
										    <%
										    	}
										    %>
										</div><!--/.form-group -->
										<div class="form-group">
											<label for="way">请输入登录方式:</label>
											<%
												if(session.getAttribute("way") == null){
											%>
											<input type="radio" name="way" value="老师">老师
											<input type="radio" name="way" value="学生">学生
											<%
												}else if(session.getAttribute("way").equals("老师")){
											%>
											<input type="radio" name="way" value="老师" checked="checked">老师
											<input type="radio" name="way" value="学生">学生
											<%
												}else{
											%>
											<input type="radio" name="way" value="老师">老师
											<input type="radio" name="way" value="学生" checked="checked">学生
											<%
												}
											%>
										</div><!--/.form-group -->
										<div class="form-group">
											<label for="password">请输入密码:</label>
											<%
												if(session.getAttribute("pass")==null){
											%>
										    <input type="password" class="form-control" id="password" name="pass" placeholder="请输入你的密码">
											<%
												}else{
											%>
											 <input type="password" class="form-control" id="password" name="pass" placeholder="请输入你的密码" value="<%=session.getAttribute("pass")%>">
											<%
												}
											%>
										</div><!--/.form-group -->
										<div class="form-group">
											<label for="repass">确认密码:</label>
											<%
												if(session.getAttribute("repass")==null){
											%>
											<input type="password" class="form-control" id="repass" name="repass" placeholder="请确认你的密码">
											<%
												}else{
											%>
											<input type="password" class="form-control" id="repass" name="repass" placeholder="请确认你的密码" value="<%=session.getAttribute("repass")%>">
											<%
												}
											%>
										</div><!--/.form-group -->
										<div class="form-group">
											<label for="email">邮箱:</label>
											<%
												if(session.getAttribute("email")==null)
												{
											%>
										    <input type="email" class="form-control" id="email" name="email" placeholder="便于密码找回">
										    <%
												}else{
										    %>
										    <input type="email" class="form-control" id="email" name="email" placeholder="便于密码找回" value="<%=session.getAttribute("email")%>">
										    <%
												}
										    %>
										    <button type="submit">邮箱验证</button>
										</div><!--/.form-group -->
										<div class="form-group">
											<label for="yzm">邮箱:</label>
											<%
												if(session.getAttribute("yzm")==null)
												{
											%>
										    <input type="text" class="form-control" id="yzm" name="yzm" placeholder="请输入你的验证码">
										    <%
												}else{
										    %>
										    <input type="text" class="form-control" id="yzm" name="yzm	" placeholder="请输入你的验证码" value="<%=session.getAttribute("yzm")%>">
										    <%
												}
										    %>
										</div><!--/.form-group -->
									</form><!--/form -->
								</div><!--/.col -->
							</div><!--/.row -->

						</div><!--/.signin-form -->

						<div class="signin-footer">
							<input type="button" id="login" value="注册" class="btn signin_btn signin_btn_two">
							<p>
								早有账号了
								<a href="signIn.jsp">登录去？</a>
							</p>
						</div><!--/.signin-footer -->
					</div><!-- .popup-in-txt -->
				</div><!--/.sign-content -->
			</div><!--/.container -->

		</section><!--/.signin -->
		
		<!-- signin end -->

		<!--footer copyright start -->
		<footer class="footer-copyright">
			<div id="scroll-Top">
				<i class="fa fa-angle-double-up return-to-top" id="scroll-top" data-toggle="tooltip" data-placement="top" title="" data-original-title="Back to Top" aria-hidden="true"></i>
			</div><!--/.scroll-Top-->

		</footer><!--/.hm-footer-copyright-->
		<!--footer copyright  end -->
		
		<!-- Include all js compiled plugins (below), or include individual files as needed -->

		<script src="../js/jquery.js"></script>
        
        <!--modernizr.min.js-->
        <script src="../js/modernizr.min.js"></script>
		
		<!--bootstrap.min.js-->
        <script src="../js/bootstrap.min.js"></script>
		
		<!-- bootsnav js -->
		<script src="../js/bootsnav.js"></script>
		
		<!-- jquery.sticky.js -->
		<script src="../js/jquery.sticky.js"></script>
		<script src="../js/jquery.easing.min.js"></script>
		
        
        <!--Custom JS-->
        <script src="../js/custom.js"></script>
		
</body>
</html>